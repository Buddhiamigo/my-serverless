const AWS = require('aws-sdk');
const AWSCognito = require('amazon-cognito-identity-js');
const util = require('util');

// const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

// AWS.CognitoIdentityServiceProvider.AuthenticationDetails = CognitoSDK.AuthenticationDetails;
// AWS.CognitoIdentityServiceProvider.CognitoUserPool = CognitoSDK.CognitoUserPool;
// AWS.CognitoIdentityServiceProvider.CognitoUser = CognitoSDK.CognitoUser;

module.exports.createUser = (event, context, callback) => {

	const data = JSON.parse(event.body);
    if(typeof data.username !== 'string' 
		&& typeof data.email !== 'string' 
		&& typeof data.password !== 'string'){
        console.error('Validation failed');
        callback(new Error('Couldn\'t add the user.'));
        return;
	}
	
	const poolData = {
		UserPoolId : process.env.IDENTITY_POOL_ID, // Your user pool id here
		ClientId : process.env.CLIENT_ID // Your client id here
	};
	
	const userPool = new AWSCognito.CognitoUserPool(poolData);

    var attributeList = [];

    var dataEmail = {
        Name : 'email',
        Value : data.email
    };

    // var dataPhoneNumber = {
    //     Name : 'phone_number',
    //     Value : '+15555555555'
    // };
    const attributeEmail = new AWSCognito.CognitoUserAttribute(dataEmail);
    // var attributePhoneNumber = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataPhoneNumber);

    attributeList.push(attributeEmail);
    // attributeList.push(attributePhoneNumber);

    userPool.signUp(data.username, data.password, attributeList, null, function(err, result){
        if (err) {
            console.log(err, err.stack);
            return;
        }
        cognitoUser = result.user;
		console.log('user name is ' + cognitoUser.getUsername());
		
		// create a response
        const response = {
			statusCode: 200,
			body: JSON.stringify(result),
		  };
		  callback(null, response);

    });
}