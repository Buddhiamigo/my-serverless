'use strict';

const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.filterCar = (event, context, callback) => {

    const params = {
        TableName: process.env.DYNAMODB_TABLE,
        FilterExpression : 'contains (brand, :brand_name)',
        ExpressionAttributeValues : {':brand_name' : event.queryStringParameters.brand}    
    };

    dynamoDb.scan(params, (error, result) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(new Error('Couldn\'t find the car item.'));
            return;
        }
    
        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(result.Items),
        };
        callback(null, response);
    });

};