'use strict';

const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.updateCar = (event, context, callback) => {
    const data = JSON.parse(event.body);
    if(typeof data.brand !== 'string' 
        && typeof data.hp !== 'number' && typeof data.type !== 'string'){
        console.error('Validation failed');
        callback(new Error('Couldn\'t add the car.'));
        return;
    }

    const params = {
        TableName: process.env.DYNAMODB_TABLE,
        Key: {
          id: event.pathParameters.id,
        },
        ExpressionAttributeValues: {
            ':brand': data.brand,
            ':hp': data.hp,
            ':model': data.model
        },
        UpdateExpression: 'SET brand = :brand, hp = :hp, model = :model',
        ReturnValues: 'ALL_NEW',
    };

    // fetch todo from the database
    dynamoDb.update(params, (error, result) => {
    // handle potential errors
        if (error) {
            console.error(error);
            callback(new Error('Couldn\'t fetch the car item.'));
            return;
        }

        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(result.Attributes),
        };
        callback(null, response);
    });
};