'use strict';

const uuid = require('uuid');
const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.addCar = (event, context, callback) => {
    const data = JSON.parse(event.body);
    if(typeof data.brand !== 'string' 
        && typeof data.id !== 'string'
        && typeof data.hp !== 'number' 
        && typeof data.type !== 'string'){
        console.error('Validation failed');
        callback(new Error('Couldn\'t add the car.'));
        return;
    }

    const params = {
        TableName: process.env.DYNAMODB_TABLE,
        Item: {
            id: uuid.v4(),
            brand: data.brand,
            hp: data.hp,
            model: data.model
        },
      };

    // write the car to the database
    dynamoDb.put(params, (error) => {
        // handle potential errors
        if (error) {
            console.error(error);
            callback(new Error('Couldn\'t add the car.'));
            return;
        }

        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(params.Item),
        };
        callback(null, response);
    });
};