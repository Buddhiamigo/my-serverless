'use strict';

const async = require('async');
const AWS = require('aws-sdk');
const uuid = require('uuid');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.asyncUpdate = (event, context, callback) => {
    const carId = event.pathParameters.carid

    async.waterfall([
        function scanCar(asyncCallback){
            const params = {
                TableName: process.env.DYNAMODB_TABLE,
                Key: {
                  id: carId
                }
            };

            dynamoDb.get(params,function(err,res){
                if (err) {
                    console.log(err);
                    callback(new Error('Async get fail'));
                    return;
                }

                asyncCallback(null,res);
            });

        },
        function addProduct(res, asyncCallback){
            console.log(res);
            const params = {
                TableName: process.env.PRODUCT_TABLE,
                Item: {
                    TenentId: uuid.v1(),
                    ProductId: res.Item.id,
                    Token: 'dummy token',
                    other_bullshit: 'jhkdkhvkajsvdkavakjajkjaksvjakjvb'
                }
            };

            dynamoDb.put(params, function(err, res){
                if(err){
                    console.log(err);
                    callback(new Error('Async put fail'));
                }
                console.log("put res :");
                console.log(res);
                asyncCallback(null,res);
            });
        }
    ],function(err,res){
        if(err){
            callback(new Error('Async fail'))
        }
        console.log("async final res :");
        console.log(res);
        callback(null,res);
    });
};